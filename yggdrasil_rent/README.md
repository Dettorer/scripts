# Rent manager for my server

    Usage: yggdrasil_rent.py [options]

    Options:
      -h, --help            show this help message and exit
      -l, --list            list all debts
      -p NAME AMOUNT, --has-paid=NAME AMOUNT
                            note payments: reduce NAME's debt and update date
      -m, --new-month       add unit_price to everybody
      -i, --irc             change colors to irc colors
