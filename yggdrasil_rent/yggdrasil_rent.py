#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from optparse import OptionParser
from os import fsync
from datetime import date, timedelta

people = []
logs = open("/home/dettorer/work/scripts/yggdrasil_rent/logs", 'r+')

# Recovering previous state via logs
for line in logs:
    [name, to_pay, last_date] = line.split()
    people.append((name, to_pay, last_date))

# Calculate the price per month and per people
# Base price: 49€
# Add three additionnal IP 1€ each : 3€
# Flexilolpack: 15€
#   - Add one /29 IPv4 subnet : 8€
# For a total of 75€
full_price = 49. + 3. + 15. + 8.
# We are currently 5, thus paying 15€ per month each.
unit_price = round(full_price/len(people), 2)

def list_people(option, opt_str, value, parser):
    """ Display current amount each people have to pay and the date of their
    last payment """

    today = date.today()

    # Find the date of the next invoice
    next_invoice = today
    while next_invoice.day != 14:
        next_invoice += timedelta(days=1)

    for person in people:
        (name, to_pay, last_date) = person
        if name == "dettorer": continue

        if len(name) < 10:
            name += '\t'

        # Amount color:
        if float(to_pay) < 0.:
            color = parser.values.irc and '\x0302' or '\033[94m' # blue
        elif float(to_pay) <= unit_price / 2:
            color = parser.values.irc and '\x0303' or '\033[92m' # green
        elif float(to_pay) <= unit_price * 2:
            color = parser.values.irc and '\x0307' or '\033[93m' # orange
        else:
            color = parser.values.irc and '\x0304' or '\033[91m' # red

        if parser.values.irc:
            endcolor = '\x03'
        else:
            endcolor = '\033[0m'

        first_tab = '\t'

        if len(to_pay) < 5: #Pretier print =D
            second_tab = '\t\t'
        else:
            second_tab = '\t'

        # Print the correct line for each people
        print "{0}{1} must pay {2}{3}€{4}{5} (Last payment: {6})".format(name, first_tab, color, to_pay, endcolor, second_tab, last_date)

    print "Everyone pay {0}€ per month, next invoice: {1} (in {2} days)".format(unit_price, next_invoice.isoformat(), (next_invoice - today).days)

def has_paid(option, opt_str, value, parser):
    i = 0
    (paid_name, amount) = value
    while i < len(people):
        [name, to_pay, last_date] = people[i]
        if name == paid_name:
            new_to_pay = repr(round(float(to_pay) - float(amount), 2))
            people[i] = (name, new_to_pay, str(date.today()))
        i += 1

def new_month(option, opt_str, value, parser):
    i = 0
    while i < len(people):
        [name, to_pay, last_date] = people[i]
        if name != 'dettorer': # I'm the one who pay the whole thing
            new_to_pay = repr(float(to_pay) + unit_price)
            people[i] = (name, new_to_pay, last_date)
        else:
            people[i] = (name, new_to_pay, str(date.today()))
        i += 1



def update_logs():
    """ Write current state in logs """
    logs = open("/home/dettorer/work/scripts/yggdrasil_rent/logs", 'w')
    logs.write('\n'.join([' '.join(person) for person in people]))

parser = OptionParser()

parser.add_option("-l", "--list",
        action="callback",
        callback=list_people,
        help="list all debts",
        )

parser.add_option("-p", "--has-paid",
        action="callback",
        callback=has_paid,
        type="string",
        metavar='NAME AMOUNT',
        help="note payments: reduce NAME's debt and update date",
        nargs=2
        )

parser.add_option("-m", "--new-month",
        action="callback",
        callback=new_month,
        help="add unit_price to everybody"
        )

parser.add_option("-i", "--irc",
        action="store_true",
        dest="irc",
        help="change colors to irc colors",
        default=False
        )


if __name__ == '__main__':
    parser.parse_args()
    update_logs()
    logs.close()

