#! /usr/bin/env sh
CURRENT=`ls -l ~/.Xdefaults | rev | cut -d_ -f1 | rev`

case $CURRENT in
    "black") NEW="white" ;;
    "white") NEW="black" ;;
esac

ln -sf ~/.Xdefaults_$NEW ~/.Xdefaults
xrdb ~/.Xdefaults
