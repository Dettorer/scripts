MAINSCREEN=DP-0
ALTSCREEN=DP-2

xrandr \
    --output $MAINSCREEN \
        --mode 2560x1440 \
        --rate 143.97 \
        --pos 1080x340 \
        --primary \
    --output $ALTSCREEN \
        --mode 1920x1080 \
        --rotate left \
        --pos 0x0 \
        --panning 0x0

with_error=`nitrogen --restore 2>&1`
without_error=`nitrogen --restore 2> /dev/null`

if [ "$with_error" != "$without_error" ]
then
    feh  --bg-scale 'FILLME'
fi
