# Keymap script

As I am a bepo user, it is anoying for everyone else who want to use my computer
occasionally, so I made this script to cycle through most common keymaps in this
order :

bépo (for my personal usage)
us intl (most of the people I know who are using qwerty use this keymap)
fr oss (some people want the keymap and the keyboard to match, this one is printed on my keyboard)
dvorak (I know some people using this keymap)

If any other keymap is set, the script will ballback on bépo (and therefore be
back in the loop).

A good idea is to make a key binding to that script (I personally use i3 and
binded it to $mod+f1)
